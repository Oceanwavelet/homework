package HackerRank.Introduction;

import java.time.LocalDate;
import java.util.Scanner;

public class JavaDateAndTime {
    public static String getDay(String day, String month, String year) {
        LocalDate date = LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));
        return date.getDayOfWeek().toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String month = in.next();
        String day = in.next();
        String year = in.next();

        System.out.println(getDay(day, month, year));
    }
}
