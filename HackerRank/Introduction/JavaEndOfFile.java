package HackerRank.Introduction;

import java.util.Scanner;

public class JavaEndOfFile {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int counter = 1;
        while (scan.hasNext()) {
            System.out.println(counter + " " + scan.nextLine());
            counter++;
        }
    }
}
