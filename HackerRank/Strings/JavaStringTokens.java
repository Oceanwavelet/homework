package HackerRank.Strings;

import java.util.Scanner;

public class JavaStringTokens {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        if (!scan.hasNext()) {
            System.out.println("0");
            return;
        }
        String s = scan.nextLine().trim();
        if (s.matches("[A-Za-z !,?._'@]+")) {
            String[] tokens = s.split("[^A-Za-z]+");
            System.out.println(tokens.length);
            for (String str : tokens) {
                System.out.println(str);
            }
        }
        else System.out.println("0");
        scan.close();
    }
}
