package HackerRank.Strings;

import java.util.Arrays;
import java.util.Scanner;

public class JavaSubstringComparisons {
    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";
        String[] arrSub = new String[s.length() - k + 1];
        for (int i = 0; i < s.length() - k + 1; i++) {
            arrSub[i] = s.substring(i, i + k);
        }
        Arrays.sort(arrSub);
        smallest = arrSub[0];
        largest = arrSub[arrSub.length - 1];


        return smallest + "\n" + largest;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();

        System.out.println(getSmallestAndLargest(s, k));
    }
}
