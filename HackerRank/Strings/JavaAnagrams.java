package HackerRank.Strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class JavaAnagrams {
    static boolean isAnagram(String a, String b) {
        if (a.length() != b.length()) return false;
        a = a.toLowerCase();
        b = b.toLowerCase();
        Map<Character, Integer> mapFreqA = new HashMap<Character, Integer>();
        Map<Character, Integer> mapFreqB = new HashMap<Character, Integer>();

        for (char ch : a.toCharArray()) {
            if (!mapFreqA.containsKey(ch)) {
                mapFreqA.put(ch, 1);
                continue;
            }
            mapFreqA.put(ch, mapFreqA.get(ch) + 1);
        }

        for (char ch : b.toCharArray()) {
            if (!mapFreqB.containsKey(ch)) {
                mapFreqB.put(ch, 1);
                continue;
            }
            mapFreqB.put(ch, mapFreqB.get(ch) + 1);
        }

        for (Character ch : mapFreqA.keySet()) {
            if (mapFreqA.get(ch) != mapFreqB.get(ch)) return false;
        }
        return true;

    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }
}
