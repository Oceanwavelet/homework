package HackerRank.Advanced;

public class JavaSingletonPattern {
    private static JavaSingletonPattern instance;
    public String str;
    private JavaSingletonPattern() {}
    static JavaSingletonPattern getSingleInstance() {
        if (instance == null) instance = new JavaSingletonPattern();
        return instance;
    }
}
