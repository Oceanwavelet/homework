package HackerRank.Advanced;

import java.io.*;
import java.util.*;
import java.security.*;

public class JavaSHA256 {

    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Scanner scan = new Scanner(System.in);
        String message = scan.next();
        byte[] bytesOfMessage = message.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] digest = md.digest(bytesOfMessage);
        for (byte b : digest) {
            System.out.printf("%02x", b);
        }
    }
}