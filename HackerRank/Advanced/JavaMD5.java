package HackerRank.Advanced;

import java.io.*;
import java.util.*;
import java.security.*;

public class JavaMD5 {

    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Scanner scan = new Scanner(System.in);
        String message = scan.next();

        byte[] bytesOfMessage = message.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(bytesOfMessage);
        for(byte b : thedigest){
            System.out.printf("%02x", b);
        }
    }
}
