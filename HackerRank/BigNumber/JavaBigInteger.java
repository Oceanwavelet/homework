package HackerRank.BigNumber;

import java.util.*;
import java.math.BigInteger;

public class JavaBigInteger {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1 = scan.nextLine();
        String str2 = scan.nextLine();
        scan.close();

        BigInteger a = new BigInteger(str1);
        BigInteger b = new BigInteger(str2);
        System.out.println(a.add(b));
        System.out.println(a.multiply(b));
    }
}