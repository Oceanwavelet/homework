package HackerRank.DataStructures;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class JavaList {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int sizeList = scan.nextInt();
        List<Integer> list = new LinkedList<Integer>();
        for (int i = 0; i < sizeList; i++) {
            list.add(scan.nextInt());
        }
        int nQuerie = scan.nextInt();
        for (int i = 0; i < nQuerie; i++) {
            if (scan.next().equals("Insert")) {
                list.add(scan.nextInt(), scan.nextInt());
            }
            else list.remove(scan.nextInt());
        }
        scan.close();
        for (Integer num : list) {
            System.out.printf("%d ", num);
        }
    }
}
