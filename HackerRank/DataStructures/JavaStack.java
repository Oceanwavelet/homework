package HackerRank.DataStructures;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class JavaStack {
    public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String input=sc.next();
            if (isBalancedString(input)) System.out.println("true");
            else System.out.println("false");
        }

    }
    static boolean isBalancedString(String str) {
        if (str.isEmpty()) return true;
        Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < str.length(); i++) {
            int len = str.length();
            char inCh = str.charAt(i);

            if (i == len - 1 && stack.isEmpty()) return false;
            if ((inCh == '(' || inCh == '[' || inCh == '{') && i != len - 1) {
                stack.addFirst(str.charAt(i));
                continue;
            }
            if (stack.isEmpty()) return false;
            char ch = stack.removeFirst();
            if ((inCh == ')' && ch == '(') ||  (inCh == ']' && ch == '[') || (inCh == '}' && ch == '{')) {
                if (i == len-1 && stack.isEmpty()) return true;
                continue;
            }
            return false;
        }
        if (!stack.isEmpty()) return false;
        return true;
    }
}
