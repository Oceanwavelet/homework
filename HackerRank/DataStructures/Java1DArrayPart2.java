package HackerRank.DataStructures;

import java.util.Scanner;

public class Java1DArrayPart2 {
    public static boolean canWin(int leap, int[] game) {
        return isSolved(0, game, leap);
    }
    private static boolean isSolved(int index, int[] game, int leap) {
        if (index < 0 || game[index] == 1) return false;
        if (index == game.length-1 || index + leap > game.length-1) return true;
        game[index] = 1;
        return isSolved(index - 1, game, leap) || isSolved(index + 1, game, leap) || isSolved(index + leap, game, leap);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

            System.out.println( (canWin(leap, game)) ? "YES" : "NO" );
        }
        scan.close();
    }
}
