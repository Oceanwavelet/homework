package HackerRank.DataStructures;

import java.util.BitSet;
import java.util.Scanner;

public class JavaBitSet {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int n = scan.nextInt();
        String operation;
        int op1;
        int op2;
        BitSet B1 = new BitSet(size);
        BitSet B2 = new BitSet(size);

        for (int i = 0; i < n; i++) {
            operation = scan.next();
            op1 = scan.nextInt();
            op2 = scan.nextInt();
            if (operation.equals("AND")) {
                if (op1 == 2) {
                    B2.and(B1);
                    System.out.println(B1.cardinality() + " " + B2.cardinality());
                    continue;
                }
                B1.and(B2);
                System.out.println(B1.cardinality() + " " + B2.cardinality());
            }
            if (operation.equals("OR")) {
                if (op1 == 2) {
                    B2.or(B1);
                    System.out.println(B1.cardinality() + " " + B2.cardinality());
                    continue;
                }
                B1.or(B2);
                System.out.println(B1.cardinality() + " " + B2.cardinality());
            }
            if (operation.equals("XOR")) {
                if (op1 == 2) {
                    B2.xor(B1);
                    System.out.println(B1.cardinality() + " " + B2.cardinality());
                    continue;
                }
                B1.xor(B2);
                System.out.println(B1.cardinality() + " " + B2.cardinality());
            }
            if (operation.equals("FLIP")) {
                if (op1 == 1) {
                    B1.flip(op2);
                    System.out.println(B1.cardinality() + " " + B2.cardinality());
                    continue;
                }
                B2.flip(op2);
                System.out.println(B1.cardinality() + " " + B2.cardinality());
            }
            if (operation.equals("SET")) {
                if (op1 == 1) {
                    B1.set(op2);
                    System.out.println(B1.cardinality() + " " + B2.cardinality());
                    continue;
                }
                B2.set(op2);
                System.out.println(B1.cardinality() + " " + B2.cardinality());
            }
        }
    }
}
