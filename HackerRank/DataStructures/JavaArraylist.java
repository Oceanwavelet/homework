package HackerRank.DataStructures;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JavaArraylist {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nLines = scan.nextInt();
        int numLine;
        int index;

        List<String[]> list = new ArrayList<>();
        for (int i = 0; i <= nLines; i++) {
            String[] str = scan.nextLine().split("\\s");
            list.add(str);
        }
        int nQueries = scan.nextInt();
        for (int i = 0; i < nQueries; i++) {
            numLine = scan.nextInt();
            index = scan.nextInt();
            try {
                System.out.println(list.get(numLine)[index]);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("ERROR!");
            }
        }
    }
}
