package HackerRank.DataStructures;

import java.util.*;

public class JavaDeque {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Deque<Integer> deque = new ArrayDeque<>();
        Set<Integer> set = new HashSet<>();
        int n = in.nextInt();
        int m = in.nextInt();
        int max = 0;
        int count = 0;
        int polled;

        for (int i = 0; i < n; i++) {
            int num = in.nextInt();
            deque.addLast(num);
            set.add(num);
            if (deque.size() != m) continue;

            count = set.size();
            if (count > max) max = count;
            polled = deque.pollFirst();
            if (!deque.contains(polled)) {
                count--;
                set.remove(polled);
            }
        }
        System.out.println(max);
    }
}
