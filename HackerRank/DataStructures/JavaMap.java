package HackerRank.DataStructures;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class JavaMap {
    public static void main(String []argh)
    {
        Scanner in = new Scanner(System.in);
        Map<String, Integer> mp = new HashMap<>();
        int n=in.nextInt();
        in.nextLine();
        for(int i=0;i<n;i++)
        {
            String name=in.nextLine();
            int phone=in.nextInt();
            in.nextLine();
            if (!mp.containsKey(name)) mp.put(name, phone);
        }
        while(in.hasNext())
        {
            String s=in.nextLine();
            if (!mp.containsKey(s)) System.out.println("Not found");
            else System.out.println(s + "=" + mp.get(s));
        }
    }
}
