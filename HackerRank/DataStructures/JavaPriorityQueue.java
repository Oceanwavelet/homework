package HackerRank.DataStructures;

import java.util.*;

public class JavaPriorityQueue {
    private final static Scanner scan = new Scanner(System.in);
    private final static Priorities priorities = new Priorities();

    public static void main(String[] args) {
        int totalEvents = Integer.parseInt(scan.nextLine());
        List<String> events = new ArrayList<>();

        while (totalEvents-- != 0) {
            String event = scan.nextLine();
            events.add(event);
        }

        List<Student> students = priorities.getStudents(events);

        if (students.isEmpty()) {
            System.out.println("EMPTY");
        } else {
            for (Student st: students) {
                System.out.println(st.getName());
            }
        }
    }
}

class Student{
    private int id;
    private String name;
    private double cgpa;
    public Student(String name, double cgpa, int id) {
        super();
        this.id = id;
        this.name = name;
        this.cgpa = cgpa;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public double getCgpa() {
        return cgpa;
    }
}

class Priorities {
    PriorityQueue<Student> pq;

    public Priorities () {
        pq = new PriorityQueue<Student>(11, new StudentComparator());
    }

    List<Student> getStudents(List<String> events) {
        List<Student> orderStudents = new ArrayList<>();
        Student addedStudent;
        for (String event : events) {
            String[] spl = event.split("\\s");
            if (spl[0].equals("SERVED")) {
                pq.poll();
            }
            if (spl[0].equals("ENTER")) {
                addedStudent = new Student(spl[1], Double.parseDouble(spl[2]), Integer.parseInt(spl[3]));
                pq.add(addedStudent);
            }
        }
        while(pq.size() != 0) {
            orderStudents.add(pq.poll());
        }
        return orderStudents;
    }

    class StudentComparator implements Comparator<Student>{

        @Override
        public int compare(Student st1, Student st2) {
            Double cgpa1 = st1.getCgpa();
            Double cgpa2 = st2.getCgpa();

            if (cgpa1.equals(cgpa2)) {
                if (st1.getName().equals(st2.getName())) return st1.getId() - st2.getId();
                return st1.getName().compareTo(st2.getName());
            }
            return -Double.compare(st1.getCgpa(), st2.getCgpa());

        }
        }

}


