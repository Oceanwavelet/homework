package CodingBat;

import java.util.HashMap;
import java.util.Map;

public class Map2 {

    /*Given an array of strings, return a Map<String, Integer> containing
    a key for every different string in the array, always with the value 0.
    For example the string "hello" makes the pair "hello":0. We'll do more
    complicated counting later, but for this problem the value is simply 0.*/

    public Map<String, Integer> word0(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String str : strings) {
            if (!map.containsKey(str)) map.put(str, 0);
        }
        return map;
    }

    /*Given an array of strings, return a Map<String, Integer> containing a key
    for every different string in the array, and the value is that string's length.*/

    public Map<String, Integer> wordLen(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String str : strings) {
            if (!map.containsKey(str)) map.put(str, str.length());
        }
        return map;
    }

    /*Given an array of non-empty strings, create and return a
    Map<String, String> as follows: for each string add its first character
    as a key with its last character as the value.*/

    public Map<String, String> pairs(String[] strings) {
        Map<String, String> map = new HashMap<String, String>();
        for (String str : strings) {
            map.put(str.substring(0, 1), str.substring(str.length() - 1));
        }
        return map;
    }

    /*The classic word-count algorithm: given an array of strings, return a
    Map<String, Integer> with a key for each different string, with the value
    the number of times that string appears in the array.*/

    public Map<String, Integer> wordCount(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String str : strings) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
                continue;
            }
            map.put(str, map.get(str) + 1);
        }
        return map;
    }

    /*Given an array of non-empty strings, return a Map<String, String> with a
    key for every different first character seen, with the value of all the strings
    starting with that character appended together in the order they appear in the array.*/

    public Map<String, String> firstChar(String[] strings) {
        Map<String, String> map = new HashMap<String, String>();
        for (String str : strings) {
            if (!map.containsKey(str.substring(0, 1))) {
                map.put(str.substring(0, 1), str);
                continue;
            }
            map.put(str.substring(0, 1), map.get(str.substring(0, 1)) + str);
        }
        return map;
    }

    /*Loop over the given array of strings to build a result string like this: when
    a string appears the 2nd, 4th, 6th, etc. time in the array, append the string to the
    result. Return the empty string if no string appears a 2nd time.*/

    public String wordAppend(String[] strings) {
        String res = "";
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String str : strings) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
                continue;
            }
            map.put(str, map.get(str) + 1);
            if (map.get(str) % 2 == 0) res += str;
        }
        return res;
    }

    /*Given an array of strings, return a Map<String, Boolean> where
    each different string is a key and its value is true if that string
    appears 2 or more times in the array.*/

    public Map<String, Boolean> wordMultiple(String[] strings) {
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        for (String str : strings) {
            if (!map.containsKey(str)) {
                map.put(str, false);
                continue;
            }
            map.put(str, true);
        }
        return map;
    }

    /*We'll say that 2 strings "match" if they are non-empty and their first chars are the
    same. Loop over and then return the given array of non-empty strings as follows: if a
    string matches an earlier string in the array, swap the 2 strings in the array. When a
    position in the array has been swapped, it no longer matches anything. Using a map, this
    can be solved making just one pass over the array. More difficult than it looks.*/

    public String[] allSwap(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].isEmpty()) continue;
            if (!map.containsKey(strings[i].substring(0, 1))) {
                map.put(strings[i].substring(0, 1), i);
                continue;
            }
            String swapStr = strings[map.get(strings[i].substring(0, 1))];
            strings[map.get(strings[i].substring(0, 1))] = strings[i];
            strings[i] = swapStr;
            map.remove(strings[i].substring(0, 1));
        }
        return strings;
    }


    /*We'll say that 2 strings "match" if they are non-empty and their first chars are
    the same. Loop over and then return the given array of non-empty strings as
    follows: if a string matches an earlier string in the array, swap the 2 strings
    in the array. A particular first char can only cause 1 swap, so once a char has
    caused a swap, its later swaps are disabled. Using a map, this can be solved
    making just one pass over the array. More difficult than it looks.*/

    public String[] firstSwap(String[] strings) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].isEmpty()) continue;
            if (!map.containsKey(strings[i].substring(0, 1))) {
                map.put(strings[i].substring(0, 1), i);
                continue;
            }
            if (map.get(strings[i].substring(0, 1)) != -1) {
                String swapStr = strings[map.get(strings[i].substring(0, 1))];
                strings[map.get(strings[i].substring(0, 1))] = strings[i];
                strings[i] = swapStr;
                map.put(strings[i].substring(0, 1), -1);
            }
        }
        return strings;
    }
}
