package CodingBat;

public class Array3 {


    /*Consider the leftmost and righmost appearances of some value in an array.
    We'll say that the "span" is the number of elements between the two inclusive.
    A single value has a span of 1.
    Returns the largest span found in the given array. (Efficiency is not a priority.)*/

    public int maxSpan(int[] nums) {
        int n = nums.length;
        int maxDist = 1;
        int dist = 1;
        if (n < 2) return n;
        for (int i = 0; i < n; i++) {
            dist = 1;
            for (int j = i + 1; j < n; j++) {
                dist++;
                if (nums[i] == nums[j] && maxDist <= dist) maxDist = dist;
            }
        }
        return maxDist;
    }


    /*Return an array that contains exactly the same numbers as the given array, but rearranged
    so that every 3 is immediately followed by a 4.
    Do not move the 3's, but every other number may move.
    The array contains the same number of 3's and 4's, every 3 has
    a number after it that is not a 3, and a 3 appears in the array before any 4.*/

    public int[] fix34(int[] nums) {
        int n = nums.length;
        int replacedNum = 0;
        for (int i = 1; i < n; i++) {
            if (nums[i-1] == 3) {
                for (int j = 0; j < n; j++) {
                    if (nums[j] == 4 && nums[j-1] != 3) {
                        replacedNum = nums[i];
                        nums[i] = nums[j];
                        nums[j] = replacedNum;
                        break;
                    }
                }
            }
        }
        return nums;
    }

    /*(This is a slightly harder version of the fix34 problem.)
    Return an array that contains exactly the same numbers as the given array,
    but rearranged so that every 4 is immediately followed by a 5. Do not move
    the 4's, but every other number may move. The array contains the same number
    of 4's and 5's, and every 4 has a number after it that is not a 4.
    In this version, 5's may appear anywhere in the original array.*/

    public int[] fix45(int[] nums) {
        int n = nums.length;
        int replacedNum = 0;
        for (int i = 1; i < n; i++) {
            if (nums[i-1] == 4) {
                for (int j = 0; j < n; j++) {
                    if (nums[j] == 5 && (j == 0 || nums[j-1] != 4)) {
                        replacedNum = nums[i];
                        nums[i] = nums[j];
                        nums[j] = replacedNum;
                        break;
                    }
                }
            }
        }
        return nums;
    }


    /*Given a non-empty array, return true if there is a place to split
    the array so that the sum of the numbers on one side is equal to
    the sum of the numbers on the other side.*/

    public boolean canBalance(int[] nums) {
        int n = nums.length;
        int sumLeft = 0;
        int sumRight = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= i) sumLeft += nums[j];
                else sumRight += nums[j];
            }
            if (sumLeft == sumRight) return true;
            sumLeft = 0;
            sumRight = 0;
        }
        return false;
    }


    /*Given two arrays of ints sorted in increasing order, outer and inner, return true if all
    of the numbers in inner appear in outer. The best solution makes only a single "linear"
    pass of both arrays, taking advantage of the fact that both arrays are already in sorted
    order.*/

    public boolean linearIn(int[] outer, int[] inner) {
        int count = 0;
        if (inner.length == 0) return true;
        int j = 0;
        int currentNum = inner[j];
        for (int i = 0; i < outer.length; i++) {
            if (outer[i] == currentNum) {
                count++;
                j++;
                if (j < inner.length) currentNum = inner[j];
                else break;
            }
            continue;
        }
        return count == inner.length;
    }


    /*Given n>=0, create an array length n*n with the following pattern,
    shown here for n=3 : {0, 0, 1,    0, 2, 1,    3, 2, 1}
    (spaces added to show the 3 groups).*/

    public int[] squareUp(int n) {
        int[] res = new int[n * n];
        int count = n + 1;
        int num = n;
        for (int i = res.length - 1; i >= 0; i -= n) {
            count--;
            num = count;
            for (int j = i - count + 1; j <= i; j++) {
                if (num != 0) {
                    res[j] = num;
                    num--;
                }
            }
        }
        return res;
    }

    /*Given n>=0, create an array
    with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n}
    (spaces added to show the grouping). Note that the length of the array
    will be 1 + 2 + 3 ... + n, which is known to sum to exactly n*(n + 1)/2.*/

    public int[] seriesUp(int n) {
        int[] res = new int[n*(n+1)/2];
        int len = res.length;
        int count = 0;
        int num = 1;
        for (int i = 0; i < len; i += count) {
            count++;
            num = 1;
            for (int j = i; j < i + count; j++) {
                if (num <= count) {
                    res[j] = num;
                    num++;
                }
            }
        }
        return res;
    }

    /*We'll say that a "mirror" section in an array is a group of contiguous
    elements such that somewhere in the array, the same group appears in reverse order.
    For example, the largest mirror section in {1, 2, 3, 8, 9, 3, 2, 1} is length
    3 (the {1, 2, 3} part). Return the size of the largest mirror section found
    in the given array.*/

    public int maxMirror(int[] nums) {
        int n = nums.length;
        int max = 0;
        int count = 0;

        for (int i = 0; i < n; i++) {
            count = 0;
            for (int j = n-1; i + count < n && j > -1; j--) {
                if (nums[i+count] == nums[j]) {
                    count++;
                }
                else {
                    max = Math.max(count, max);
                    count = 0;
                }
            }
            max = Math.max(count, max);
        }
        return max;
    }


    /*Say that a "clump" in an array is a series of 2 or more adjacent elements of
    the same value. Return the number of clumps in the given array.*/

    public int countClumps(int[] nums) {
        int n = nums.length;
        int count = 0;
        for (int i = 0; i < n - 1; i++) {
            if (nums[i] == nums[i+1]) {
                for (int j = i+1; j < n; j++) {
                    if (j == n-1 && nums[i] == nums[j]) {
                        count++;
                        i = j-1;
                        break;
                    }
                    if (nums[j] != nums[i]) {
                        count++;
                        i = j-1;
                        break;
                    }
                }
            }
        }
        return count;
    }
}
