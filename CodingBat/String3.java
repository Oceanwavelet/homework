package CodingBat;

public class String3 {

    /*Given a string, count the number of words ending in 'y' or 'z' --
    so the 'y' in "heavy" and the 'z' in "fez" count, but not the 'y' in
    "yellow" (not case sensitive).
    We'll say that a y or z is at the end of a word if there is not an alphabetic
    letter immediately following it. (Note: Character.isLetter(char)
    tests if a char is an alphabetic letter.)*/

    public int countYZ(String str) {
        str = str.toLowerCase();
        int count = 0;
        String[] splitStr = str.split("[\\W\\d]");
        for (int i = 0; i < splitStr.length; i++) {
            if (splitStr[i].endsWith("y") || splitStr[i].endsWith("z")) count++;
        }
        return count;
    }


    /*Given two strings, base and remove, return a version of the base string where
    all instances of the remove string have been removed (not case sensitive).
    You may assume that the remove string is length 1 or more. Remove only non-overlapping
    instances, so with "xxx" removing "xx" leaves "x".*/

    public String withoutString(String base, String remove) {
        String removeLo = remove.toLowerCase();
        String removeUP = remove.toUpperCase();
        String pattern = "(" + removeLo + ")|(" + removeUP + ")|(" + remove + ")";
        return base.replaceAll(pattern, "");
    }

    /*Given a string, return true if the number of appearances of "is"
    anywhere in the string is equal to the number of appearances of "not"
    anywhere in the string (case sensitive).*/

    public boolean equalIsNot(String str) {
        int countIs = 0;
        int countNot = 0;
        for (int i = 0; i <= str.length() - 2; i++) {
            if (str.substring(i, i+2).equals("is")) countIs++;
            if (i < str.length() - 2 && str.substring(i, i+3).equals("not")) countNot++;
        }
        return countIs == countNot;
    }

    /*We'll say that a lowercase 'g' in a string is "happy" if there is another 'g'
    immediately to its left or right. Return true if all the g's in the given string are happy.*/

    public boolean gHappy(String str) {
        String removed2gStr = str.replaceAll("(ggg)|(gg)", "");
        return removed2gStr.matches("[^g]*");
    }

    /*We'll say that a "triple" in a string is a char appearing three times in a row.
    Return the number of triples in the given string. The triples may overlap.*/

    public int countTriple(String str) {
        int count = 0;
        if (str.length() < 3) return 0;
        for (int i = 0; i < str.length() - 2; i++) {
            String triple = str.substring(i, i+1) + str.substring(i, i+1) + str.substring(i, i+1);
            if (str.substring(i, i+3).equals(triple)) count++;
        }
        return count;
    }


    /*Given a string, return the sum of the digits 0-9 that appear in the string,
    ignoring all other characters.
    Return 0 if there are no digits in the string. (Note: Character.isDigit(char)
    tests if a char is one of the chars '0', '1', .. '9'. Integer.parseInt(string)
    converts a string to an int.)*/

    public int sumDigits(String str) {
        String strNoLetters = str.replaceAll("\\D", "");
        int result = 0;
        for (char ch : strNoLetters.toCharArray()) {
            result += Character.getNumericValue(ch);
        }
        return result;
    }

    /*Given a string, return the longest substring that appears at both the beginning
    and end of the string without overlapping. For example, sameEnds("abXab") is "ab".*/

    public String sameEnds(String string) {
        int n = string.length();
        String result = "";
        int indexMid = indexOfMidStr(string);

        if (n < 2) return result;
        String left = string.substring(0, n / 2);
        String right = string.substring(indexMid);
        for (int i = left.length(), j = 0; i > 0; i--) {
            if (left.substring(0, i).equals(right.substring(j))) {
                result = left.substring(0, i);
                break;
            }
            j++;
        }
        return result;
    }

    public int indexOfMidStr (String str) {
        if (str.length() % 2 == 0) return str.length()/2;
        return str.length() / 2 + 1;
    }

    /*Given a string, look for a mirror image (backwards) string at both the beginning and end
    of the given string. In other words, zero or more characters at
    the very begining of the given string, and at the very end of the string
    in reverse order (possibly overlapping).
    For example, the string "abXYZba" has the mirror end "ab".*/

    public String mirrorEnds(String string) {
        int n = string.length();
        String result = "";
        String reverceStr = "";

        for (int i = n - 1; i >= 0; i--) {
            reverceStr += string.substring(i, i+1);
        }
        for (int i = n; i >= 0; i--) {
            if (string.substring(0, i).equals(reverceStr.substring(0, i))) {
                result = string.substring(0, i);
                break;
            }
        }
        return result;
    }

    /*Given a string, return the length of the largest "block" in the string.
    A block is a run of adjacent chars that are the same.*/

    public int maxBlock(String str) {
        int maxBlock = 0;
        int n = str.length();
        int count = 1;
        char[] charStr = str.toCharArray();

        if (n == 0) return 0;
        for (int i = 1; i < n; i++) {
            if (charStr[i] == charStr[i-1]) {
                count++;
            }
            else {
                if (maxBlock < count) maxBlock = count;
                count = 1;
            }
        }
        if (maxBlock < count) maxBlock = count;
        return maxBlock;
    }

    /*Given a string, return the sum of the numbers appearing in the string,
    ignoring all other characters. A number is a series of 1 or more digit
    chars in a row. (Note: Character.isDigit(char) tests if a char is one of
    the chars '0', '1', .. '9'. Integer.parseInt(string) converts a string to an int.)*/

    public int sumNumbers(String str) {
        int sum = 0;
        String[] splitStr = str.split("(\\D+)");

        for (int i = 0; i < splitStr.length; i++) {
            if (splitStr[i].equals("")) continue;
            sum += Integer.parseInt(splitStr[i]);
        }
        return sum;
    }


    /*Given a string, return a string where every appearance of the lowercase word "is" has
    been replaced with "is not". The word "is" should not be immediately preceeded or followed
    by a letter -- so for example the "is" in "this" does not count. (Note: Character.isLetter(char)
    tests if a char is a letter.)*/

    public String notReplace(String str) {
        return str.replaceAll("(?<!\\w)(is)(?!\\w)", "is not");
    }








}
