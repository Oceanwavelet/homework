package CodingBat;

public class String2 {

    /*Given a string, return a string where for every char in the original, there are two chars.*/

    public String doubleChar(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            result += str.substring(i, i+1) + str.substring(i, i+1);
        }
        return result;
    }


    /*Return the number of times that the string "hi" appears anywhere in the given string.*/

    public int countHi(String str) {
        int result = 0;
        for (int i = 0; i < str.length()-1; i++) {
            if (str.substring(i, i+2).equals("hi")) result++;
        }
        return result;
    }

    /*Return true if the string "cat" and "dog" appear the same number of times in the given string.*/

    public boolean catDog(String str) {
        int countCats = 0;
        int countDogs = 0;
        for (int i = 0; i < str.length()-2; i++) {
            if (str.substring(i, i+3).equals("cat")) {
                countCats++;
                continue;
            }
            if (str.substring(i, i+3).equals("dog")) {
                countDogs++;
                continue;
            }
        }
        return countDogs == countCats;
    }


    /*Return the number of times that the string "code" appears anywhere in the given string,
    except we'll accept any letter for the 'd', so "cope" and "cooe" count.*/

    public int countCode(String str) {
        int result = 0;
        for (int i = 0; i < str.length()-3; i++) {
            if (str.substring(i, i+4).matches("co\\we")) result++;
        }
        return result;
    }

    /*Given two strings, return true if either of the strings appears at the very
    end of the other string, ignoring upper/lower
    case differences (in other words, the computation should not be "case sensitive").
    Note: str.toLowerCase() returns the lowercase version of a string.*/

    public boolean endOther(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();
        return a.endsWith(b) || b.endsWith(a);
    }

    /*Return true if the given string contains an appearance of "xyz" where the xyz
    is not directly preceeded by a period (.). So "xxyz" counts but "x.xyz" does not.*/

    public boolean xyzThere(String str) {
        return str.matches("(.*[^\\.]xyz.*)|(xyz.*)");
    }


    /*Return true if the given string contains a "bob" string,
    but where the middle 'o' char can be any char.*/

    public boolean bobThere(String str) {
        return str.matches(".*b.b.*");
    }

    /*We'll say that a String is xy-balanced if for all the 'x' chars in the string,
    there exists a 'y' char somewhere later in the string. So "xxy" is balanced,
    but "xyx" is not. One 'y' can balance multiple 'x's. Return true if the
    given string is xy-balanced.*/

    public boolean xyBalance(String str) {
        return str.matches("(.*x.*y[^x]*)|([^x]*)");
    }

    /*Given two strings, a and b, create a bigger string made of the first char of a,
    the first char of b, the second char of a, the second char of b, and so on.
    Any leftover chars go at the end of the result.*/

    public String mixString(String a, String b) {
        int lenA = a.length();
        int lenB = b.length();
        String result = "";
        for (int i = 0; i < Math.max(lenA, lenB); i++) {
            if (i < lenA && i < lenB) {
                result += a.substring(i, i+1) + b.substring(i, i+1);
                continue;
            }
            if (i == lenA) {
                result += b.substring(i);
                break;
            }
            else {
                result += a.substring(i);
                break;
            }
        }
        return result;
    }

    /*Given a string and an int n, return a string made of n repetitions of the last
    n characters of the string. You may assume that n is between 0 and the length of the string,
    inclusive.*/

    public String repeatEnd(String str, int n) {
        String result = "";
        int lenStr = str.length();
        for (int i = 0; i < n; i++) {
            result += str.substring(lenStr - n);
        }
        return result;
    }

    /*Given a string and an int n, return a string made of the first n characters of the string,
    followed by the first n-1 characters of the string, and so on.
    You may assume that n is between 0 and the length of the string,
    inclusive (i.e. n >= 0 and n <= str.length()).*/

    public String repeatFront(String str, int n) {
        String result = "";
        for (int i = 0; i < n; i++) {
            result += str.substring(0, n-i);
        }
        return result;
    }

    /*Given two strings, word and a separator sep, return
    a big string made of count occurrences of the word, separated by the separator string.*/

    public String repeatSeparator(String word, String sep, int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            if (i == count - 1) {
                result += word;
                break;
            }
            result += word + sep;
        }
        return result;
    }


    /*Given a string, consider the prefix string made of the first N chars of the string.
    Does that prefix string appear somewhere else in the string?
    Assume that the string is not empty and that N is in the range 1..str.length().*/

    public boolean prefixAgain(String str, int n) {
        String prefix = str.substring(0, n);
        return str.substring(n).matches(".*" + prefix + ".*");
    }

    /*Given a string, does "xyz" appear in the middle of the string? To define middle,
    we'll say that the number of chars to the left and right of the "xyz" must differ
    by at most one. This problem is harder than it looks.*/

    public boolean xyzMiddle(String str) {
        int n = str.length();
        if (n < 3) return false;
        if (n % 2 == 1) {
            if (str.substring(n/2 - 1, n/2 + 2).equals("xyz")) return true;
        }
        if (n % 2 == 0) {
            if (str.substring(n/2 - 2, n/2 + 2).indexOf("xyz") != -1) return true;
        }
        return false;
    }

    /*A sandwich is two pieces of bread with something in between.
    Return the string that is between the first and last appearance of "bread"
    in the given string, or return the empty string "" if there are not two pieces of bread.*/

    public String getSandwich(String str) {
        String result = "";
        if (str.matches(".*bread.*bread.*")) {
            int firstBr = str.indexOf("bread");
            int secondBr = str.lastIndexOf("bread");
            result += str.substring(firstBr+5, secondBr);
        }
        return result;
    }

    /*Returns true if for every '*' (star) in the string, if there are chars
    both immediately before and after the star, they are the same.*/

    public boolean sameStarChar(String str) {
        for (int i = 1; i < str.length() - 1; i++) {
            if (str.charAt(i) == '*' && str.charAt(i-1) != str.charAt(i+1)) return false;
        }
        return true;
    }

    /*Given a string, compute a new string by moving the first char to come
    after the next two chars, so "abc" yields "bca". Repeat this process for each
    subsequent group of 3 chars, so "abcdef" yields "bcaefd".
    Ignore any group of fewer than 3 chars at the end.*/

    public String oneTwo(String str) {
        int n = str.length();
        String result = "";
        if (n > 2) {
            for (int i = 0; i < n-2; i+=3) {
                result += str.substring(i+1, i+3);
                result += str.substring(i, i+1);
            }
        }
        return result;
    }

    /*Look for patterns like "zip" and "zap" in the string -- length-3,
    starting with 'z' and ending with 'p'. Return a string where for all
    such words, the middle letter is gone, so "zipXzap" yields "zpXzp".*/

    public String zipZap(String str) {
        String result = "";
        if (str.length() < 3) return str;
        for (int i = 0; i < str.length(); i++) {
            if (i < str.length() - 2 && str.substring(i, i+3).matches("z\\wp")) {
                result += "zp";
                i+=2;
                continue;
            }
            result += str.substring(i, i+1);
        }
        return result;
    }

    /*Return a version of the given string, where for every star (*)
    in the string the star and the chars immediately to its left and right are gone.
    So "ab*cd" yields "ad" and "ab**cd" also yields "ad".*/

    public String starOut(String str) {
        String[] splitStr = str.split(".?\\*+.?");
        String result = "";
        for (int i = 0; i < splitStr.length; i++) {
            result += splitStr[i];
        }
        return result;
    }

    /*Given a string and a non-empty word string, return a
    version of the original String where all chars have been replaced
    by pluses ("+"), except for appearances of the word
    string which are preserved unchanged.*/

    public String plusOut(String str, String word) {
        int n = word.length();
        int l = str.length();
        String result = "";
        for (int i = 0; i < l; i++) {
            if (i < (l - n + 1) && str.substring(i, i+n).equals(word)) {
                result += word;
                i += n - 1;
                continue;
            }
            result += "+";
        }
        return result;
    }

    /*Given a string and a non-empty word string, return a string made of each char
    just before and just after every appearance of the word in the string.
    Ignore cases where there is no char before or after the word, and a char may be
    included twice if it is between two words.*/

    public String wordEnds(String str, String word) {
        int n = word.length();
        int l = str.length();
        String result = "";
        for (int i = 0; i <= l - n; i++) {
            if (i - 1 >= 0  && str.substring(i, i+n).equals(word)) {
                result += str.substring(i-1, i);
            }
            if (i + n < l && str.substring(i, i+n).equals(word)) {
                result += str.substring(i+n, i+n+1);
            }
        }
        return result;
    }
}
